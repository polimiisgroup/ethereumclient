in order for images to be properly built, dockerfiles expect the following folder structure:

project root folder (folder)
|
+-Dockerfile-blockclient (file)
+-Dockerfile-ipfsblockclient (file)
+-EthereumClient (folder)
| |
| +-blockclient (folder)
| +-ipfsblockclient (folder)
| +-testdata (folder)
|
+-paso (folder)