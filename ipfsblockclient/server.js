const express = require('express');
const app = express();
const port = 3000 || process.env.PORT;
const web3 = require('web3');
const SocketServer = require('ws').Server;
var connectedUsers = [];
var messages;
const bodyParser = require('body-parser');
var Client = require('node-rest-client').Client;
var client = new Client();
var USER = 'admin';
var PASS = 'password';
var clientId = 'ntsblock_' + Math.random().toString(16).substr(2, 8);
var BrokerHost = 'localhost';
var BrokerPort = 1883;
var uuid = require('node-uuid');
var remoteArtifacts = [];
var mappingPath = 'mapping.xml';
var xml2js = require('xml2js');
var fs = require('fs');
var crypto = require("crypto");
const IPFS = require('ipfs');
var ipfsAPI = require('ipfs-api')
const node = new IPFS();
var contract = require('./build/contracts/IPFSblockclient.json');
var contractAddress = null;
var account = null;


var externalcep = false;
var artifactId;
var artifactName;
web3js = new web3(new web3.providers.HttpProvider("http://localhost:7545"));

if(process.argv.length > 2) {
	contractAddress = contract.networks[process.argv[2]].address;
} else {
	console.log('network id is required');
	process.exit(0);
}
if(process.argv.length > 3) {
	account = process.argv[3];
} else {
	account = web3js.eth.accounts[0];
} 
var contractABI = web3js.eth.contract(contract.abi);
var contractInstance = contractABI.at(contractAddress);
console.log(web3js.eth.accounts);
console.log(contractInstance);


var event = contractInstance.LogWrittenHash({},{fromBlock:"latest", toBlock:"latest"});
event.watch(function(error,result){
	console.log(result);
	console.log(result.args._mHash);
	var msg = result.args._mHash;
	node.files.cat(msg, function (err, file) {
  				if (err) {
    					throw err
  					}
  					
						client.methods.notifyPASO({parameters: {name: (JSON.parse(file.toString())).event.payloadData.topic, value: (JSON.parse(file.toString())).event.payloadData.status, timestamp: (JSON.parse(file.toString())).event.payloadData.timestamp}}, function (data, response) {
							});
						})
});


client.registerMethod("sendDataToLocalCEP", "http://localhost:1880/api/data", "GET");

client.registerMethod("notifyPASO", "http://localhost:8083/api/updateInfoModel", "GET");

client.registerMethod("getInfoModel", "http://localhost:8083/api/infoModel", "GET");

app.get('/api/rereadbinding', function (req, res) {
   var parseString = xml2js.parseString;
    parseString(fs.readFileSync(mappingPath, 'utf8'), function (err, result) {
      //TODO, aggiungere la gestione dell'errore
      console.log('parsed');
	  initConnections(result);
    });
   res.end('Binding file reread');
})

app.get('/api/simulateevent', function (req, res) {
	if (req.param('eventid')!=undefined){
		//find if the event is responsible for communicating a change in an artifact instance
          for (var artifact in remoteArtifacts){
              for (var br in remoteArtifacts[artifact].bindingEvents){
                  if(req.param('eventid') == remoteArtifacts[artifact].bindingEvents[br]) {
                      //event body has a field indicating the new artifact instance
                      if(req.param('data')!=undefined) {
                        if(remoteArtifacts[artifact].id != req.param('data')){
        										remoteArtifacts[artifact].id = req.param('data');
                          }
                      }
                  }

              }
							for (var ur in remoteArtifacts[artifact].unbindingEvents){
								if(req.param('eventid') == remoteArtifacts[artifact].unbindingEvents[ur]) {
                      remoteArtifacts[artifact].id = '';
                  }
              }
							}

          client.methods.notifyPASO({parameters: {name: (req.param('eventid')), value: ''}}, function (data, response) {

          });
	}
	res.end('Event sent');
})

app.get('/api/data', function (req, res) {
   var obj = {}
   obj['id'] = 'foobar';
   obj['timestamp'] = Date.now();

   for(var key in req.query) {
       obj[key] = req.param(key);
   }
    if(externalcep) {
       objPayload.payloadData = obj;
       objEvent.event = objPayload;
        res.end(JSON.stringify(objEvent));
   } else {
       var args = {};
       args['parameters'] = obj;
       console.log(JSON.stringify(args));
        client.methods.sendDataToLocalCEP(args, function (data, response) {

        });
       res.end('CEPInvoked');
   }
})

app.get('/api/status', function (req, res) {
   if(!externalcep) {
        //publish status on MQTT for other artifacts
        var objContainer = {};
        objContainer.id = 'foobar';
        objContainer.status = req.param('status');
        objContainer.timestamp = Date.now();
        objPayload.payloadData = objContainer;
        objEvent.event = objPayload;
		//send status to PASO
        client.methods.notifyPASO({parameters: {name: artifactName, value: req.param('status')}}, function (data, response) {
        		 // parsed response body as js object
	           console.log(data);
	           // raw response
	           console.log(response);
              });

        res.end(JSON.stringify(objEvent));
   } else {
        res.end('NotActive');
   }
})


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use('/', express.static('public_static'));

app.get('/',function(req,res){
  res.sendfile("reread.html");
});

var server = app.listen(port, function () {
	
    console.log('node.js static, REST server and websockets listening on port: ' + port)
});
const wss = new SocketServer({ server });

wss.on('connection', function connection(ws) {
    console.log("connection ...");
		connectedUsers.push(ws);

		ws.on('message', function incoming(message) {
  			console.log('received: %s', message);
  			
  			var id = crypto.randomBytes(5).toString('hex');
					fs.writeFile(id, message, (err) => {
	    if (err) {
	        console.error(err);
	        return;
	    };
	    console.log("File has been created");

	});
	var fileToRetrieve ="";
		
	node.files.add({
			path: id,
			content: Buffer.from(message)
				}, function(err,res){
					if(err){
						console.error(err);
					}else{

						console.log('Added file:', res[0].path, res[0].hash)
						fileToRetrieve = (res[0].hash).toString();
						console.log(fileToRetrieve);
						contractInstance.writeHash((JSON.parse(message.toString())).event.payloadData.topic ,fileToRetrieve,{from:account,gas: 2000000,gasPrice: web3js.toWei(20, 'gwei')});

					}
			});
	});
});

function initConnections(mapping) {
	console.log('startmap');
	for (var la in mapping['martifact:definitions']['martifact:localArtifact']){
		artifactName=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].name;
		artifactId=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].id;
		externalcep=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].externalCep;
	}
	var ra = mapping['martifact:definitions']['martifact:mapping'][0]['martifact:artifact'];
    remoteArtifacts = [];
	for(var artifact in ra) {
      var br = [];
      for (var aid in ra[artifact]['martifact:bindingEvent']){
          br.push(ra[artifact]['martifact:bindingEvent'][aid]['$'].id);
      }
	  var ur = [];
      for (var aid in ra[artifact]['martifact:unbindingEvent']){
          ur.push(ra[artifact]['martifact:unbindingEvent'][aid]['$'].id);
      }
      remoteArtifacts.push({name: ra[artifact]['$'].name, id: '', bindingEvents: br, unbindingEvents: ur});
	}
    var stakeHolders = mapping['martifact:definitions']['martifact:stakeholder'];
}

var objPayload = {};
var objEvent = {};
