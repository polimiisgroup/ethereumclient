var IPFSblockclient = artifacts.require("./IPFSblockclient.sol");

module.exports = function(deployer, networks, accounts) {
  var processModelHash = "QmNk6bz6qF487d6WYhXVWBLJ6hBSk1uQ3LZLwnoTEi3Meu";
  deployer.deploy(IPFSblockclient, processModelHash, [accounts[0]], [web3.fromAscii("Truck")],
  {from: accounts[0], gas:6721975, value: 5000000000000000000}
  );
  };
  