pragma solidity ^ 0.4 .18;


contract IPFSblockclient {

	string currentProcessModelHash;
	struct StateHash {
		uint id;
		string mHash;
	}
	
	struct participant {
	    bytes32 encodedTopic;
	    address participantAddress;
	}
	
	mapping(uint => StateHash) public hashes;
	uint hashCounter;
	mapping(address => participant) public currentParticipants;
	mapping(uint => address) currentParticipantsIndex;
	uint currentParticipantCounter;
	
	event LogInfo(
		string description
	);
	event LogMsgUpdate(
		string result
	);
	event LogWrittenHash(
		uint indexed _id,
		string _mHash
	);

	// Constructor
	function IPFSblockclient(string processModelHash, address[] participantsAddress, bytes32[] encodedTopics) payable public {
		for (uint p = 0; p < participantsAddress.length; p++) {
			currentParticipants[participantsAddress[p]].participantAddress = participantsAddress[p];
			currentParticipants[participantsAddress[p]].encodedTopic = encodedTopics[p];
			currentParticipantsIndex[p] = participantsAddress[p];
			currentParticipantCounter++;
		}
		currentProcessModelHash = processModelHash;
	}

	// Fallback function
	function () public {
		revert();
	}

	function writeHash(string _topic, string _mHash) payable public {
		if (currentParticipants[msg.sender].encodedTopic == stringToBytes32(_topic)) {
			//increment state counter
			hashCounter++;
			//store a state
			hashes[hashCounter] = StateHash(
				hashCounter,
				_mHash
			);
			LogWrittenHash(hashCounter, _mHash);
		}
	}

	function getNumberOfHashes() public view returns(uint) {
		return hashCounter;
	}

	function getParticipants() public view returns(address[]) {
        address[] memory result = new address[](currentParticipantCounter);
        for (uint i = 0; i < currentParticipantCounter; i++){
            result[i] = currentParticipantsIndex[i];
        }
        return result;
    }
    
    function getCounter() public view returns(uint){
        return currentParticipantCounter;
    }
    
    function getParticipant(address participantAddress) public view returns(string) {
        return bytes32ToString(currentParticipants[participantAddress].encodedTopic);
    }
	
	function getPastHash(uint position) public view returns(string) {
		return hashes[position].mHash;
	}
	
	function getProcessModelHash() public view returns(string) {
		return currentProcessModelHash;
	}
	
	function stringToBytes32(string memory source) private returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}

	function bytes32ToString(bytes32 x) constant returns (string) {
    bytes memory bytesString = new bytes(32);
    uint charCount = 0;
    for (uint j = 0; j < 32; j++) {
        byte char = byte(bytes32(uint(x) * 2 ** (8 * j)));
        if (char != 0) {
            bytesString[charCount] = char;
            charCount++;
        }
    }
    bytes memory bytesStringTrimmed = new bytes(charCount);
    for (j = 0; j < charCount; j++) {
        bytesStringTrimmed[j] = bytesString[j];
    }
    return string(bytesStringTrimmed);
}


}