// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*'
    },
    private: {
      host: '127.0.0.1',
      port: 8545,
      network_id: '4224',
      from: '0x987fa4a68432c175e090b935670e1fa232d00daf',
      gas: 6721975
    },
rinkeby:{
host: '127.0.0.1',
port: 8545,
network_id: '4',
from: '0xaa3b9753ee0bcc63728649e9a0b8579b833d0db6',
gas: 6721975
}
  }
}
