#!/bin/sh

# Prepare ganache-cli
ganache-cli -p 7545 -i 5777 &

# Deploy contract
cd /blockclient
truffle migrate --compile-all --reset --network ganache

# Prepare PASO
cd /paso
cp /testdata/siena.xml /paso/data/test
cp /testdata/infoModel.xsd /paso/data/test
node server.js &

# Prepare node-red
mkdir /node-red
cd /node-red
cd /node-red/node_modules/node-red
cp /testdata/LHR_AMS_C_12345.csv /node-red/node_modules/node-red
cp /testdata/LHR_AMS_NC_98765.csv /node-red/node_modules/node-red
node red.js &

# Wait for node-red to start
sleep 10

# Deploy node-red flow
cd /testdata
curl -X POST http://localhost:1880/flows -H "Content-Type: application/json" --data "@nodered.txt"

# Prepare blockclient
cd /blockclient
node server.js 5777
