pragma solidity ^0.4.18;
pragma experimental  ABIEncoderV2;

contract Blockclient {

	string currentProcessModel;
	//custom types
	struct State {
		uint id;
		address sender;
		string topic;
		string status;
		string timestamp;
		string data;
	}
	
	struct participant {
	    bytes32 encodedTopic;
	    address participantAddress;
	}
	
	//state variables
	mapping(address => participant) public currentParticipants;
	mapping(uint => address) currentParticipantsIndex;
	uint currentParticipantCounter;
	mapping(uint => State) public states;
	uint stateCounter;

	// events
	event LogWriteState(
		uint indexed _id,
		address indexed _sender,
		string _topic,
		string _status,
		string _timestamp,
		string _data
	);

	function Blockclient(string processModel, address[] participantsAddress, bytes32[] encodedTopics
	) payable public {
		for (uint p = 0; p < participantsAddress.length; p++) {
			currentParticipants[participantsAddress[p]].participantAddress = participantsAddress[p];
			currentParticipants[participantsAddress[p]].encodedTopic = encodedTopics[p];
			currentParticipantsIndex[p] = participantsAddress[p];
			currentParticipantCounter++;
		}
		currentProcessModel = processModel;
	}

	// Fallback function
	function () public {
		revert();
	}
	
	function stringToBytes32(string memory source) private returns (bytes32 result) {
    bytes memory tempEmptyStringTest = bytes(source);
    if (tempEmptyStringTest.length == 0) {
        return 0x0;
    }

    assembly {
        result := mload(add(source, 32))
    }
}


	function writeState(string _topic, string _status, string _timestamp, string _data) payable public {
		if (currentParticipants[msg.sender].encodedTopic == stringToBytes32(_topic)
		)
		{
			//increment state counter
			stateCounter++;
			//store a state
			states[stateCounter] = State(
				stateCounter,
				msg.sender,
				_topic,
				_status,
				_timestamp,
				_data
			);

			LogWriteState(stateCounter, msg.sender, _topic, _status, _timestamp, _data);
		}
	}

	function getNumberOfStates() public view returns(uint) {
		return stateCounter;
	}

    function getParticipants() public view returns(address[]) {
        address[] memory result = new address[](currentParticipantCounter);
        for (uint i = 0; i < currentParticipantCounter; i++){
            result[i] = currentParticipantsIndex[i];
        }
        return result;
    }
    
    function getCounter() public view returns(uint){
        return currentParticipantCounter;
    }
    
    function getParticipant(address participantAddress) public view returns(string) {
        return bytes32ToString(currentParticipants[participantAddress].encodedTopic);
    }
	
	function getPastState(uint position) public view returns(State) {
		return states[position];
	}
	
	function getProcessModel() public view returns(string) {
		return currentProcessModel;
	}
	
	function bytes32ToString(bytes32 x) constant returns (string) {
    bytes memory bytesString = new bytes(32);
    uint charCount = 0;
    for (uint j = 0; j < 32; j++) {
        byte char = byte(bytes32(uint(x) * 2 ** (8 * j)));
        if (char != 0) {
            bytesString[charCount] = char;
            charCount++;
        }
    }
    bytes memory bytesStringTrimmed = new bytes(charCount);
    for (j = 0; j < charCount; j++) {
        bytesStringTrimmed[j] = bytesString[j];
    }
    return string(bytesStringTrimmed);
}
}