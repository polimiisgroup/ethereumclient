App = {
  web3Provider: null,
  contracts: {},
  account: 0x0,
  loading: false,

  init: function() {
	console.log('init');
    return App.initWeb3();
  },

  initWeb3: function() {
    // initialize web3
    if(typeof web3 !== 'undefined') {
      //reuse the provider of the Web3 object injected by Metamask
      App.web3Provider = web3.currentProvider;
      console.log('your provider is:' + ' ' + App.web3Provider);
    } else {
      //create a new provider and plug it directly into our local node
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);
    console.log(web3.eth.accounts);
    App.displayAccountInfo();
    console.log('your provider is:'+ ' ' + web3.toString());
    return App.initContract();

},

displayAccountInfo: function() {
    web3.eth.getCoinbase(function(err, account) {
      if(err === null) {
        App.account = account;
        $('#account').text(account);
        web3.eth.getBalance(account, function(err, balance) {
          if(err === null) {
            $('#accountBalance').text(web3.fromWei(balance, "ether") + " ETH");
          }
        })
      }
    });
  },


  writeState: function(){
    var _topic = $('#statusMsg').text();
    var _status = $('#timestampMsg').text();
    var _timestamp = $('#topicMsg').text();


    App.contracts.TruffleExpress.deployed().then(function(instance){
      return instance.writeState(_topic, _status, _timestamp, {from: App.account, gas:6721975});
    }).then(function(result){
      console.log(result);
    }).catch(function(error){
      console.error(error);

    });
App.displayAccountInfo();
$("#events").empty();

  },

  initContract: function() {

      $.getJSON('https://api.myjson.com/bins/1cbdn2', function(truffleExpressArtifact) {
        // get the contract artifact file and use it to instantiate a truffle contract abstraction
        App.contracts.TruffleExpress = TruffleContract(truffleExpressArtifact);
        // set the provider for our contracts
        App.contracts.TruffleExpress.setProvider(App.web3Provider);
        //start listening
        App.listenToEvents();

      });
    },

reloadPastEvent: function(){
  var contractInstance;
    App.contracts.TruffleExpress.deployed().then(function(instance){
      contractInstance = instance;
      return contractInstance.getPastStates();
    }).then(function(stateIds){
      stateIds.forEach(function(element){
        var stateId = element;
        contractInstance.states(stateId.toNumber()).then(function(state){
          console.log(state);
          App.displayPastEvent(state[0],state[1], state[2],state[3],state[4]);
        })
      });
}).catch(function(err) {
      console.error(err.message);
    });
},

displayPastEvent: function(id, sender, status, timestamp, topic) {
  $("#events").append('<li class="list-group-item"> <b>Id:</b>' + id + '<br> <b>Sender:</b> ' + sender + '<br><b>Status:</b>' + status+ '<br><b>Timestamp:</b>'+ timestamp+'<br><b> Topic:</b>Truck</li>');
},




  listenToEvents: function() {
    //listen to event writeState
    App.contracts.TruffleExpress.deployed().then(function(instance) {
      instance.LogWriteState({}, {}).watch(function(error, event) {
        if (!error) {
          $('#statusLog').text(event.args._topic);
          $('#timestampLog').text(event.args._status);
          $('#topicLog').text(event.args._timestamp);
          $('#titleLog').text('Block:'+ ' '+event.blockNumber);

          //create event to notify client ws
          var event = new Event('bc');
          dispatchEvent(event);
          App.reloadPastEvent();
        } else {
          console.error(error);
        }

      })
    });
  },

  websocketConnect: function(){
              var ws = new WebSocket((window.location.protocol == 'https:' ? 'wss://' : 'ws://')
                  + window.location.host);
                  console.log('conncected to'+ ' ' +window.location.host);
               ws.onopen = function () {

               };
               //listener to event bc
               addEventListener('bc',function(){
                  var status =  $('#statusLog').text();
                  var timestamp =  $('#timestampLog').text();
                  var topic =   $('#topicLog').text();
                  //if the event is different from the last written it means is new and it's sent back to the server

                    var msg = {'id':'update','status':status,'timestamp':timestamp,'topic':topic};
                    var string = JSON.stringify(msg);
                      ws.send(string);

                })
                //when receiving a message display it and write it on the blockchain
              ws.onmessage = function (message) {

                    var res0 = JSON.parse(message.data);
                    console.log(res0);
                    $('#titleMsg').text('New message arrived!');
                    $('#statusMsg').text(res0.event.payloadData.status);
                    $('#timestampMsg').text(res0.event.payloadData.timestamp );
                    $('#topicMsg').text(res0.event.payloadData.topic);
                    App.writeState();

                  };
                }

};
//start function: init the provider and start the client ws
$(function() {
  $(document).ready(function() {
    App.init();
    App.websocketConnect();
})


});
