const express = require('express');
const app = express();
const port = 3000 || process.env.PORT;
const Web3 = require('web3');
const SocketServer = require('ws').Server;
var connectedUsers = [];
var messages;
const bodyParser = require('body-parser');
var Client = require('node-rest-client').Client;
var client = new Client();
var USER = 'admin';
var PASS = 'password';
var clientId = 'ntsblock_' + Math.random().toString(16).substr(2, 8);
var BrokerHost = 'localhost';
var BrokerPort = 1883;
var uuid = require('node-uuid');
var remoteArtifacts = [];
var mappingPath = 'mapping.xml';
var xml2js = require('xml2js');
var fs = require('fs');

if(process.argv.length > 4) {
	BrokerHost = process.argv[2];
	USER = process.argv[3];
	PASS = process.argv[4];
}

if(process.argv.length > 5) {
    mappingPath = process.argv[5];
}

var opts = { host: BrokerHost, port: BrokerPort, username: USER, password : PASS, keepalive: 30, clientId: clientId };
var externalcep = false;
var artifactId;
var artifactName;

client.registerMethod("sendDataToLocalCEP", "http://localhost:1880/api/data", "GET");

client.registerMethod("notifyPASO", "http://localhost:8083/api/updateInfoModel", "GET");

client.registerMethod("getInfoModel", "http://localhost:8083/api/infoModel", "GET");

app.get('/api/rereadbinding', function (req, res) {
   var parseString = xml2js.parseString;
    parseString(fs.readFileSync(mappingPath, 'utf8'), function (err, result) {
      //TODO, aggiungere la gestione dell'errore
      console.log('parsed');
	  initConnections(result);
    });
   res.end('Binding file reread');
})

app.get('/api/simulateevent', function (req, res) {
	if (req.param('eventid')!=undefined){
		//find if the event is responsible for communicating a change in an artifact instance
          for (var artifact in remoteArtifacts){
              for (var br in remoteArtifacts[artifact].bindingEvents){
                  if(req.param('eventid') == remoteArtifacts[artifact].bindingEvents[br]) {
                      //event body has a field indicating the new artifact instance
                      if(req.param('data')!=undefined) {
                        if(remoteArtifacts[artifact].id != req.param('data')){
        										remoteArtifacts[artifact].id = req.param('data');
                          }
                      }
                  }

              }
							for (var ur in remoteArtifacts[artifact].unbindingEvents){
								if(req.param('eventid') == remoteArtifacts[artifact].unbindingEvents[ur]) {
                      remoteArtifacts[artifact].id = '';
                  }
              }
							}

          client.methods.notifyPASO({parameters: {name: (req.param('eventid')), value: ''}}, function (data, response) {

          });
	}
	res.end('Event sent');
})

app.get('/api/data', function (req, res) {
   var obj = {}
   obj['id'] = 'foobar';
   obj['timestamp'] = Date.now();

   for(var key in req.query) {
       obj[key] = req.param(key);
   }
    if(externalcep) {
       objPayload.payloadData = obj;
       objEvent.event = objPayload;
        res.end(JSON.stringify(objEvent));
   } else {
       var args = {};
       args['parameters'] = obj;
       console.log(JSON.stringify(args));
        client.methods.sendDataToLocalCEP(args, function (data, response) {

        });
       res.end('CEPInvoked');
   }
})

app.get('/api/status', function (req, res) {
   if(!externalcep) {
        //publish status on MQTT for other artifacts
        var objContainer = {};
        objContainer.id = 'foobar';
        objContainer.status = req.param('status');
        objContainer.timestamp = Date.now();
        objPayload.payloadData = objContainer;
        objEvent.event = objPayload;
		//send status to PASO
        client.methods.notifyPASO({parameters: {name: artifactName, value: req.param('status')}}, function (data, response) {
        		 // parsed response body as js object
	           console.log(data);
	           // raw response
	           console.log(response);
              });

        res.end(JSON.stringify(objEvent));
   } else {
        res.end('NotActive');
   }
})


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use('/', express.static('public_static'));

app.get('/',function(req,res){
  res.sendfile("index.html");
});
var server = app.listen(port, function () {
    console.log('node.js static, REST server and websockets listening on port: ' + port)
});
const wss = new SocketServer({ server });
wss.on('connection', function connection(ws) {
    console.log("connection ...");
		connectedUsers.push(ws);

		ws.on('message', function incoming(message) {
  			console.log('received: %s', message);
				if(message.charAt(2)=='e'){
					connectedUsers.forEach(ws => {
									ws.send(message);

							 });
							 console.log('sent');

			//	client.methods.notifyPASO({parameters: {name: "Truck", value: (JSON.parse(message.toString())).event.payloadData.status, timestamp: (JSON.parse(message.toString())).event.payloadData.timestamp}}, function (data, response) {
			//	 	});
				}else{
					console.log(message);
					var res = JSON.parse(message);

				client.methods.notifyPASO({parameters: {name: "Truck", value: (JSON.parse(message.toString())).status, timestamp: (JSON.parse(message.toString())).timestamp}}, function (data, response) {

				});
				}

    });
	});

function initConnections(mapping) {
	console.log('startmap');
	for (var la in mapping['martifact:definitions']['martifact:localArtifact']){
		artifactName=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].name;
		artifactId=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].id;
		externalcep=mapping['martifact:definitions']['martifact:localArtifact'][la]['$'].externalCep;
	}
	var ra = mapping['martifact:definitions']['martifact:mapping'][0]['martifact:artifact'];
    remoteArtifacts = [];
	for(var artifact in ra) {
      var br = [];
      for (var aid in ra[artifact]['martifact:bindingEvent']){
          br.push(ra[artifact]['martifact:bindingEvent'][aid]['$'].id);
      }
	  var ur = [];
      for (var aid in ra[artifact]['martifact:unbindingEvent']){
          ur.push(ra[artifact]['martifact:unbindingEvent'][aid]['$'].id);
      }
      remoteArtifacts.push({name: ra[artifact]['$'].name, id: '', bindingEvents: br, unbindingEvents: ur});
	}
    var stakeHolders = mapping['martifact:definitions']['martifact:stakeholder'];
}

var objPayload = {};
var objEvent = {};
