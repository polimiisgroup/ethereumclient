// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
    ganache: {
      host: '127.0.0.1',
      port: 7545,
      network_id: '*'
    },
    private: {
      host: '127.0.0.1',
      port: 8545,
      network_id: '4224',
      from: '0x987fa4a68432c175e090b935670e1fa232d00daf',
      gas: 6721975
    },
    rinkeby: {
      host: '127.0.0.1',
      port: 8545,
      network_id: 4, //id for rinkeby,
      from: '0x7FBBc98288C2429B27Bd94c08AC0f018abe1bf57',
      gas: 6721975
    }
  }
}
