pragma solidity ^0.4.18;

contract TruffleExpress {

//custom types
struct State{
  uint id;
  address sender;
  string topic;
  string status;
  string timestamp;
}
//state variables

mapping(uint => State) public states;
uint stateCounter;


function TruffleExpress() payable public{

}

// events
event LogWriteState(
  uint indexed _id,
  address indexed _sender,
  string _topic,
  string _status,
  string _timestamp
);

function writeState(string _topic, string _status, string _timestamp) payable public {

  //increment state counter
    stateCounter++;
//store a state
    states[stateCounter] = State(
      stateCounter,
      msg.sender,
      _topic,
      _status,
      _timestamp
      );

      LogWriteState(stateCounter,msg.sender, _topic, _status, _timestamp);
  }

  function getNumberOfStates() public view returns (uint) {
    return stateCounter;
  }

  // fetch and return all article IDs for articles still for sale
  function getPastStates() public view returns (uint[]) {
    // prepare output array
    uint[] memory stateIds = new uint[](stateCounter);

    uint numberOfStates = 0;
    // iterate over articles
    for(uint i = 1; i <= stateCounter;  i++) {
        stateIds[numberOfStates] = states[i].id;
        numberOfStates++;

    }
    // copy the first array into a smaller one
    uint[] memory pastState = new uint[](numberOfStates);
    for(uint j = 0; j < numberOfStates; j++) {
      pastState[j] = stateIds[j];
    }
    return pastState;
  }

}

/* function getState() public view returns(
  address _sender,
  string _topic,
  string _status,
  string _timestamp
)  {
    return (sender, topic, status, timestamp);
} */
