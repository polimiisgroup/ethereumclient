# Blockchain clients

Software components to publish artifact-related events to the Ethereum blockchain:

* blockclient: Fully blockchain-based client
* ipfsblockclient: DFS-blockchain hybrid client

Deprecated test clients:

* experimental-blockclient: Fully blockchain-based client (requires web browser to work)
* experimental-ipfsblockclint: DFS-blockchain hybrid client (requires web browser to work)

# Quickstart

* Run either blockclient or ipfsblockclient docker image (takes 1-2 minutes to start):
```
#!batch
docker run -p 1880:1880 -p 8083:8083 gmeroni/smartifact-blockclient
```
or
```
#!batch
docker run -p 1880:1880 -p 8083:8083 gmeroni/smartifact-ipfsblockclient
```

* With a web browser, reach http://localhost:1880
* Click on the button on the left of either the "compliant" or the "noncompliant" process log
* With a web browser, reach http://localhost:8083
* Click on the section "Graphical view" and observe the process being monitored
* To restart the simulation, restart the docker container

# Introductory videos:

* blockclient: http://purl.org/polimi/martifact/blockclient
* ipfsblockclient: http://purl.org/polimi/martifact/ipfsblockclient

# Prerequisites

* PASO E-GSM Engine: https://bitbucket.org/polimiisgroup/egsmengine
* Node-Red
* Node.js 10.5
* Truffle 4.0.4
* Ganache 1.3
* Python 2.7
* (on Windows): Visual Studio 2015 (C++ development environment already configured)

# Case study configuration

## Prerequisites configuration

* Copy files testdata/infoModel.xsd and testdata/processModel.xml into PASO E-GSM Engine folder
* Launch PASO E-GSM Engine (see PASO readme file)
* Load the process model indicating processModel.xml and infoModel.xsd, respectively, as process model and data model

* Copy files testdata/LHR_AMS_C_12345.csv and testdata/LHR_AMS_NC_98765.csv into node-red folder
* Launch node-red and create a new flow
* With a web browser, reach node-red console at http://localhost:1880
* With a text editor, open file testdata/nodered.txt
* Copy all the contents and, from node-red console, select import-->clipboard, then paste the contents
* Deploy the imported flow clicking the Deploy button

## blockclient or ipfsblockclient configuration

* Launch Ganache
* Annotate the network ID (typically 5777)
* Within a console terminal/command prompt, navigate to blockclient (or ipfsblockclient) folder, then execute the following command: 
```
#!batch
truffle.cmd console --network ganache 
```
(truffle console opens)
* Within Truffle console, execute the following command: 
```
#!batch
migrate --compile-all --reset
```
* Within Ganache interface, go to section Transactions, locate the first transaction labeled "contract creation" and annotate the contract address (field "Created contract address")
* Within a console terminal/command prompt, navigate to blockclient (or ipfsblockclient) folder, then execute the following commands: 
```
#!batch
npm install
node server.js [networkID] [accountID]
```
Replace networkID with the previously annotated network ID, and accountID with the participant account ID (choose any of the accounts specified in Ganache)

* With a web browser, reach node-red console at http://localhost:1880
* Activate either compliant or noncompliant trigger buttons to start the simulation
* Observe the output in PASO (sections PASO and Graphical view) and Ganache (sections Blocks and Transactions)

## troubleshooting

In case ipfsblockclient returns the error "Lock file is already being hold" immediately after being started, navigate to the home directory of the current user (typically /home/user on Unix or C:\Users\user on Windows) and delete the folder named .jsipfs

## experimental-blockclient configuration

* Launch Ganache
* With a web browser, reach the following web address: http://www.myjson.com (will be used to publish the smart contract)
* Within a console terminal/command prompt, navigate to blockclient folder, then execute the following command: 
```
#!batch
truffle.cmd console --network ganache (truffle console opens)
```
* Within Truffle console, execute the following command: 
```
#!batch
migrate --compile-all --reset
```
* With a text editor, open the generated file blockclient/build/contracts/TruffleExpress.json
* Copy all the contents, paste them into myjson.com, publish the document and annotate the URI

* With a text editor, open file blockclient/public_static/javascript/app.js and replace the URI at row 65
* Within a console terminal/command prompt, navigate to blockclient folder, then execute the following commands: 
```
#!batch
npm install
node server.js
```
* With a web browser, reach blockclient console page at http://localhost:3000

* With a web browser, reach node-red console at http://localhost:1880
* Activate either compliant or noncompliant trigger buttons to start the simulation
* Observe the output in PASO (section Graphical view), Ganache, and blockclient console

## experimental-ipfsblockclient configuration

* Launch Ganache
* With a web browser, reach the following web address: http://www.myjson.com (will be used to publish the smart contract)
* Within a console terminal/command prompt, navigate to ipfsblockclient folder, then execute the following command: 
```
#!batch
truffle.cmd console --network ganache (truffle console opens)
```
* Within Truffle console, execute the following command: 
```
#!batch
migrate --compile-all --reset
```
* With a text editor, open the generated file ipfsblockclient/build/contracts/TruffleExpress.json
* Copy all the contents, paste them into myjson.com, publish the document and annotate the URI

* With a text editor, open file ipfsblockclient/public_static/javascript/app.js and replace the URI at row 65
* Within a console terminal/command prompt, navigate to ipfsblockclient folder, then execute the following commands: 
```
#!batch
npm install
node server.js
```
* With a web browser, reach ipfsblockclient console page at http://localhost:3000

* With a web browser, reach node-red console at http://localhost:1880
* Activate either compliant or noncompliant trigger buttons to start the simulation
* Observe the output in PASO (section Graphical view), Ganache, and ipfsblockclient console