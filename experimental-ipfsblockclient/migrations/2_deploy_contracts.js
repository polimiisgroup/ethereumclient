var IPFSblockclient = artifacts.require("./IPFSblockclient.sol");

module.exports = function(deployer, network, accounts) {
  // Deploys the OraclizeIPFS contract and funds it with 50 ETH
  // The contract needs a balance > 0 to communicate with Oraclize
  deployer.deploy(
    IPFSblockclient,
    { from: accounts[0], gas:6721975, value: 1000000000000000000 });
};
