pragma solidity ^0.4.18;


contract IPFSblockclient {


  string public msgHash;
    struct StateHash{
      uint id;
      address sender;
      string mHash;
    }

    mapping(uint => StateHash) public hashes;
    uint hashCounter;

    event LogInfo(
      string description
      );
    event LogMsgUpdate(
      string result
      );
    event LogWrittenHash(
      uint indexed _id,
      address indexed _sender,
      string _mHash
      );

    // Constructor
    function IPFSblockclient() payable public {


    }

    // Fallback function
    function() public{
        revert();
    }





    function writeHash(string _mHash) payable public {
      //increment state counter
    hashCounter++;
//store a state
    hashes[hashCounter] = StateHash(
      hashCounter,
      msg.sender,
      _mHash
      );

      LogWrittenHash(hashCounter, msg.sender, _mHash);
    }

    function getNumberOfHashes() public view returns (uint) {
    return hashCounter;
  }

  // fetch and return all article IDs for articles still for sale
  function getPastHashes() public view returns (uint[]) {
    // prepare output array
    uint[] memory hashIds = new uint[](hashCounter);

    uint numberOfHash = 0;
    // iterate over articles
    for(uint i = 1; i <= hashCounter;  i++) {
        hashIds[numberOfHash] = hashes[i].id;
        numberOfHash++;

    }
    // copy the first array into a smaller one
    uint[] memory pastHashes = new uint[](numberOfHash);
    for(uint j = 0; j < numberOfHash; j++) {
      pastHashes[j] = hashIds[j];
    }
    return pastHashes;
  }



}
