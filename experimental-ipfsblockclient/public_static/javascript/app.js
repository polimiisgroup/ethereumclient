App = {
  web3Provider: null,
  contracts: {},
  account: 0x0,

  init: function() {
    return App.initWeb3();
  },

  initWeb3: function() {
    // initialize web3
    if(typeof web3 !== 'undefined') {
      //reuse the provider of the Web3 object injected by Metamask
      App.web3Provider = web3.currentProvider;
      console.log('your provider is:' + ' ' + App.web3Provider);
    } else {
      //create a new provider and plug it directly into our local node
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:7545');
    }
    web3 = new Web3(App.web3Provider);
    web3.eth.defaultAccount = web3.eth.accounts[9];
    console.log('default accunt is:' + ' ' +web3.eth.defaultAccount);
    console.log(web3.eth.accounts);
    App.displayAccountInfo();
    console.log('your provider is:'+ ' ' + web3.toString());
    return App.initContract();
},

displayAccountInfo: function() {
    web3.eth.getCoinbase(function(err, account) {
      if(err === null) {
        App.account = account;
        $('#account').text(account);
        web3.eth.getBalance(account, function(err, balance) {
          if(err === null) {
            $('#accountBalance').text(web3.fromWei(balance, "ether") + " ETH");
          }
        })
      }
    });



  },

//

  initContract: function() {

      $.getJSON('https://api.myjson.com/bins/6v2ki', function(truffleExpressArtifact) {
        // get the contract artifact file and use it to instantiate a truffle contract abstraction
        App.contracts.IPFSblockclient = TruffleContract(truffleExpressArtifact);
        // set the provider for our contracts
        App.contracts.IPFSblockclient.setProvider(App.web3Provider);
      //  App.reloadMessages();
      //  App.listenToEvents();
      console.log(web3.eth.defaultAccount);
      App.listenToEvents();
      });
    },




  listenToEvents: function() {
    App.contracts.IPFSblockclient.deployed().then(function(instance) {
      instance.LogMsgUpdate({}, {}).watch(function(error, event) {
        if (!error) {
          console.log(event);
           var res = JSON.parse(event.args.msgHash);
           alert(res);
        } else {
          console.error(error);
        }

      })
    });
    App.contracts.IPFSblockclient.deployed().then(function(instance) {
      instance.LogWrittenHash({}, {}).watch(function(error, event) {
        if (!error) {
          //instance.update(event.args._mHash);
          console.log(event);
          $('#titleLog').text("Hash written on block number" + event.blockNumber);
          $('#hashLog').text(event.args._mHash);
          var event = new Event('bc');
          dispatchEvent(event);
          App.reloadPastEvent();
        } else {
          console.error(error);
        }

      })
    });
  },
  websocketConnect: function(){
              var ws = new WebSocket((window.location.protocol == 'https:' ? 'wss://' : 'ws://')
                  + window.location.host);
              ws.onopen = function () {
                console.log('conncected');
                };
               addEventListener('bc',function(){
                  var hash =  $('#hashLog').text();
                      ws.send(hash);
                      console.log("happen");
                    })
              ws.onmessage = function (message) {
                    var _msgHash = (message.data);
                    console.log(_msgHash);
                    if($('#hashLog').text() == _msgHash){
                      console.log('wrong status');
                    }else{
                      $('#titleMsg').text('Message received');
                      $('#hashMsg').text(_msgHash);
                      App.writeHash();
                    }
                  }
                },

writeHash: function(){
    var _msgHash = $('#hashMsg').text();
  App.contracts.IPFSblockclient.deployed().then(function(instance){
    instance.writeHash(_msgHash,{from: App.account, gas:6721975});
}).then(function(result){
      console.log(result);
    }).catch(function(error){
      console.error(error);
    });
    App.displayAccountInfo();
    $("#events").empty();
},

reloadPastEvent: function(){
  var contractInstance;
    App.contracts.IPFSblockclient.deployed().then(function(instance){
      contractInstance = instance;
      return contractInstance.getPastHashes();
    }).then(function(hashIds){
      hashIds.forEach(function(element){
        var hashId = element;
        contractInstance.hashes(hashId.toNumber()).then(function(hash){
          //console.log(hash);
          App.displayPastHashes(hash[0],hash[1], hash[2]);
        })
      });
}).catch(function(err) {
      console.error(err.message);
    });
},

displayPastHashes: function(id, sender, hash) {
  $("#events").append('<li class="list-group-item"> <b>Id:</b>' + id + '<br> <b>Sender:</b> ' + sender + '<br><b>Hash:</b>' + hash + '<br><b> Topic:</b>Truck <br> </li>');
},



};




$(function() {
  $(document).ready(function() {
    App.init();
    App.websocketConnect();
})


});
