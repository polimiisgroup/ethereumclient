module.exports = {
  networks: {
    ganache: {
      host: "localhost",
      port: 7545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      host: '127.0.0.1',
      port: 8545,
      network_id: 4, //id for rinkeby,
      from: '0x7FBBc98288C2429B27Bd94c08AC0f018abe1bf57',
      gas: 6721975
    }
  }
};
